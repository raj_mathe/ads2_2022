# Vorlesungswoche 2 (11.–17. April 2022) #

## Agenda ##

- [x] Themen:
  - topologische Sortierung existiert gdw. Graph azyklisch
  - Traversierung: Breiten- vs. Tiefensuche
  - Starke zshgd Komponente mittels Tarjan-Algorithmus bestimmen.
    - Traversierung
    - Ausgaben von `v / in[v] / l[v]`.
    - Ausgaben von Komponenten.
