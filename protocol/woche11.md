# Vorlesungswoche 11 (13.–19. Juni 2022) #

## Agenda ##

- [x] Rucksackproblem
  - Greedyalgorithmus
  - Branch & Bound

## Implementierung ##

Implementierung zum Austesten findet man in [code/python](./../code/python). _Beachte:_

- Der Code ist etwas ineffizient implementiert,
  weil extra (unnötige) Informationen zwecks schöner Darstellung gespeichert wird.
  Die Schritte sind jedoch korrekt.
- Man kann die Aspekte der Probleme
  in dem [assets](./../code/python/assets) Ordner anpassen,
  wie bspw. ob alle Gewichte und alle Summen angezeigt werden sollen.

In den Beispielen wird _mehr als nötig_ ausgegeben. Die extra Spalten sowie die etwas unnötig ausführlich aufgeschriebenen Summen sollen nur didaktischen Zwecken dienen. Sobald man alles kapiert hat, kann man selbstverständlich auf die zusätzlichen Teile verzichten.
