# Vorlesungswoche 13 (27. Juni – 3. Juli 2022) #

## Agenda ##

- [x] ein wenig Algebra:
  - [x] modulare Arithmetik
  - [x] zyklische (Unter)gruppen; Anwendung `ф(n) := ord((ℤ/nℤ)ˣ)`.
  - [x] der kleine Fermat: `a^ф(n) ≡ 1 mod n`.
- [x] Euklid:
  - [x] Rekursives Ausrechnen von `d := ggT(a,b)`.
  - [x] Extrahieren der Koeffizienten für die Gleichung `d = xa + yb`.
  - [x] Anwendung auf Sonderfall `d = 1` für Inversion modulo `n`.
- [x] Pollard-Rho
  - [x] Was liefert der Algorithmus?
  - [x] Was garantiert das?
  - Referenzen:
    - Algorithmus im Kurs (exp-Abstände), siehe:
      </br>
      <http://staff.ustc.edu.cn/~csli/graduate/algorithms/book6/chap33.htm>
    - Andere Variante (lineare Abstände), siehe:
      </br>
      <https://link.springer.com/article/10.1007/BF01933667>
      und
      </br>
      <https://www.cambridge.org/core/journals/mathematical-proceedings-of-the-cambridge-philosophical-society/article/theorems-on-factorization-and-primality-testing/6762E84DBD34AEF13E6B1D1A8334A989>

### Nächste Woche/n ###

- Wiederholung zur Klausurvorbereitung
