# Vorlesungswoche 14 (4.–10. Juli 2022) #

## Agenda ##

- Wiederholung
  - Netze/Flüsse: Restgraphen, Push, Relabel.
  - Gomory-Hu
  - Branch & Bound: Rucksack, TSP.
  - Matrixmultkiplikation
  - Matroiden, Greedy
  - Genetischer Algorithmus

### Nächste Woche/n ###

- Wiederholung zur Klausurvorbereitung
