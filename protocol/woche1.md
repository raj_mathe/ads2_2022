# Vorlesungswoche 1 (4.–10. April 2022) #

## Agenda ##

- [x] Orga
  - Ziel von Seminaren vs. Übung
  - Repo
- [x] Themen:
  - Einige (nicht alle) Begriffe über Graphen
  - Weg vs. Pfad vs. Zyklus
  - Zusammenhang:
    - (schwach) zusammenhängend ⟺ ∀u,v ∈ V(G): ∃ Weg von entweder u nach v ODER v nach u
    - **stark** zusammenhängend ⟺ ∀u,v ∈ V(G): ∃ Weg von u nach v
    </br>
    ⟺ ∀u,v ∈ V(G): ∃ Weg von sowohl u nach v UND v nach u
  - Für unger. Gph: zshgd ⟺ stark zshgd. Aber nicht für ger. Gph.
  - induzierter Teilgraph
  - stark zshgd Komponente
    - Konstruktion
    - maximale Komponenten

### TODOs (Studierende) ###

- Begriffe aus VL1 **sorgfältig** (siehe Anmerkung im Skript!) aufschreiben, verinnerlichen, sich damit vertraut machen.
- abzugebende Übungsblätter zu Ende machen.
