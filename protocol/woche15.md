# Vorlesungswoche 15 (11.–17. Juli 2022) #

## Agenda ##

- Wiederholung
  - Alle genetischen Algorithmen im Kurs, inkl. letzter VL, Bsp. aus freiwilligen Übungsblättern.
  - Branch & Bound: Rucksack, TSP.
  - Dynamisches Programmieren.
  - Matroiden: Versprecher letzte Woche
    </br>
    ```
    Ich sagte:
    |A| < |B| ==> es gibt x ∈ E s.d. A u {x} ∈ M

    Das ist zwar nicht falsch, aber nicht die ganze Aussage.
    Es hätte so lauten sollen:
    |A| < |B| ==> es gibt x ∈ B\A s.d. A u {x} ∈ M
    ```
  - Pollard-Rho: Die Rolle der Primzahlen und die Funktion `φ(n)`.

### Anmerkung ###

Wir haben mal kurz über Editoren für Notizen geredet. Einerseits gibt es Word (iiiiii) und **LaTeX** (schönes Rendering, nicht so schöne Syntax).
Dann gibt es immer plaintext. Aber eine Stufe besser ist **Markdown**. Markdown hat die folgenden Vorteile:

- Nur plaintext Dateien (d.h. keine riesige XML-Objekte im Hintergrund).
- Sehr simple Syntax (bspw. `**fett**`, `_kursiv_`, usw.).
- **Keine Kompilation** nötig—nur ein guter Renderer.
- Komplexe Textformats mit sehr intuitiver Syntax möglich:
  - Überschriften
  - Listen
  - Tabellen. Z. B. man gibt
    ```
    | Objekt | Kosten | Wert |
    | :----: | -----: | ---: |
    | Apfel  | 1,30€  |  301 |
    | Orange | 2€     |  440 |
    ```
    ein und bekommt

    | Objekt | Kosten | Wert |
    | :----: | -----: | ---: |
    | Apfel  | 1,30€  |  301 |
    | Orange | 2€     |  440 |

    raus.
  - Code Blocks (in diversen Sprachen). Z. B. man gibt
    ```
     '''java
     public class HelloWorld {
         public static void main (String[] args) {
             System.out.println("Hello World!");
         }
     }
     '''
    ```
    (mit Backticks statt `'''`) und bekommt
    ```java
    public class HelloWorld {
        public static void main (String[] args) {
            System.out.println("Hello World!");
        }
    }
    ```
    raus.

  - URIs
  - interne Verlinkungen!
  - sogar Mathe-Formeln mit (simplem) LaTeX-Code

Man braucht lediglich einen Renderer für Vorschau-Zwecken.
Hier ein paar Empfehlungen:

- VSCode + passende markdown Extensions (alles gratis)
- Typora (nicht gratis, aber für mich hat es sich gelohnt). Siehe bspw. <https://support.typora.io/Math>. Der Editor rendert sogar LaTeX-Formeln, und hat einen WYSIWYG-Modus.
- online Editoren wie
  - https://cryptpad.linxx.net -> Markdown-Folien
  - https://dillinger.io
- In Git-Repos (wie dieser hier!) werden **md**-Dateien automatisch gerendert!

### Nächste Woche/n ###

- Keine Übung, sondern die Klausur! Viel Erfolg!
