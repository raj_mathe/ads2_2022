# Vorlesungswoche 10 (6.–12. Juni 2022) #

## Agenda ##

- [x] Hirschbergalgorithmus

Implementierung zum Austesten findet man in [code/python](./../code/python). _Beachte:_

- Der Code ist etwas ineffizient implementiert,
  weil extra (unnötige) Informationen zwecks schöner Darstellung gespeichert wird.
  Die Schritte sind jedoch korrekt.
- Man kann die Aspekte der Probleme
  in dem [assets](./../code/python/assets) Ordner anpassen,
  wie bspw. die Prioritäten der Bewegungen innerhalb der Editmatrix.
