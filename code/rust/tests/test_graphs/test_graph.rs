// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

use rstest::fixture;
use rstest::rstest;
use std::panic::catch_unwind;

use ads2::graphs::graph;
use ads2::*;

// ----------------------------------------------------------------
// Fixtures
// ----------------------------------------------------------------

#[fixture]
fn fixture_graph() -> graph::Graph<i32> {
    return graph::Graph::new(
        vec![1,2,3,4,5,6,7,8],
        vec![(1,2), (1, 3), (2,3), (3,4), (4, 5), (5, 6), (6, 2), (6, 7), (6, 8)]
    );
}

// ----------------------------------------------------------------
// Test Graph
// ----------------------------------------------------------------

#[test]
fn test_graph_creation() {
    assert!(catch_unwind(|| {
        let gph = graph::Graph::new(
            vec![5, 7, 8],
            vec![(5,7), (7, 8)]
        );
        assert_length!(gph, 3);
        let gph = graph::Graph::new(
            vec!["5", "7", "8", "10"],
            vec![("5", "7"), ("7", "8")]
        );
        assert_length!(gph, 4);
        let gph = graph::Graph::<f64>::new(Vec::new(), Vec::new());
        assert_length!(gph, 0);
    }).is_ok());
}

#[rstest]
fn test_graph_subgraph(fixture_graph: graph::Graph<i32>) {
    let sub_gph = fixture_graph.subgraph(vec![2,4,5,6,8]);
    assert_eq_contents!(sub_gph.edges, vec![(6,2), (4,5), (5,6), (6,8)]);
}

#[rstest]
fn test_graph_successors_and_predecessors(fixture_graph: graph::Graph<i32>) {
    assert_eq_contents!(fixture_graph.successors(&1), vec![2, 3]);
    assert_length_unique!(fixture_graph.successors(&8), 0);
    assert_eq_contents!(fixture_graph.successors(&6), vec![2, 7, 8]);

    assert_length_unique!(fixture_graph.predecessors(&1), 0);
    assert_eq_contents!(fixture_graph.predecessors(&8), vec![6]);
    assert_eq_contents!(fixture_graph.predecessors(&6), vec![5]);
}
