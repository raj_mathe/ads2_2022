// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

use std;
extern crate closure;

use ads2::stacks::stack;

// ----------------------------------------------------------------
// Test stack
// ----------------------------------------------------------------

#[test]
fn test_stack_initialisation() {
    let stack = stack::Stack::<i32>::new();
    assert_eq!(stack.len(), 0);
}

#[test]
fn test_stack_push() {
    let mut stack = stack::Stack::<String>::new();
    assert_eq!(stack.len(), 0);
    stack.push("hallo".to_string());
    assert_eq!(stack.len(), 1);
    stack.push("welt".to_string());
    assert_eq!(stack.len(), 2);
    stack.push("!".to_string());
    assert_eq!(stack.len(), 3);
}

#[test]
fn test_stack_no_error() {
    let mut stack = stack::Stack::<String>::new();
    stack.push("hallo".to_string());
    stack.push("welt".to_string());
    stack.push("!".to_string());
    let result = std::panic::catch_unwind(closure::closure!(move mut stack, || {
        stack.pop();
        stack.pop();
        stack.pop();
    }));
    assert!(result.is_ok());
}

#[test]
fn test_stack_error_po() {
    let mut stack = stack::Stack::<String>::new();
    stack.push("hallo".to_string());
    stack.push("welt".to_string());
    stack.push("!".to_string());
    let result = std::panic::catch_unwind(closure::closure!(move mut stack, || {
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
    }));
    assert!(result.is_err());
}
