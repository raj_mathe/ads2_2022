// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

use ads2::core::utils;
use ads2::value_min;

// ----------------------------------------------------------------
// Test regex
// ----------------------------------------------------------------

#[test]
fn test_regex() {
    let re = utils::construct_regex("^a+(.*?)bc");
    assert_eq!(re.is_match("aaa---bc"), true);
    assert_eq!(re.is_match("aaa---b"), false);
}

// ----------------------------------------------------------------
// Test min
// ----------------------------------------------------------------

#[test]
fn test_min() {
    assert_eq!(value_min!("beth", "alef"), "alef");
    assert_eq!(value_min!("alef", "beth"), "alef");
    assert_eq!(value_min!("alef", "ale"), "ale");
    assert_eq!(value_min!(320, 24), 24);
    assert_eq!(value_min!(0.03, 0.2), 0.03);
}

// ----------------------------------------------------------------
// Test vector methods
// ----------------------------------------------------------------

#[test]
pub fn test_restrict() {
    let x: Vec<String> = vec![String::from("a"), String::from("b"), String::from("b"), String::from("c"), String::from("d"), String::from("e")];
    assert_eq!(utils::restrict(&x, 0, 0), Vec::<String>::new());
    assert_eq!(utils::restrict(&x, 2, 2), Vec::<String>::new());
    assert_eq!(utils::restrict(&x, 0, 1), vec!["a"]);
    assert_eq!(utils::restrict(&x, 2, 4), vec!["b", "c"]);
    let x: Vec<i32> = vec![78, 100, -3, 1];
    assert_eq!(utils::restrict(&x, 0, 0), Vec::<i32>::new());
    assert_eq!(utils::restrict(&x, 4, 4), Vec::<i32>::new());
    assert_eq!(utils::restrict(&x, 1, 3), vec![100, -3]);
    let mut x_restr = utils::restrict(&x, 1, 3);
    x_restr[0] = 58;
    assert_ne!(x[1], 58, "Result should be a copy.");
    assert_eq!(x[1], 100, "Result should be a copy.");
}

#[test]
pub fn test_remove_first_last() {
    let x: Vec<String> = vec!["merkur", "venus", "mars", "terra", "jupiter", "saturn", "uranus", "neptun"]
            .iter()
            .map(|s| {s.to_string()})
            .collect::<Vec<String>>();
    assert_eq!(utils::remove_first(&x), vec!["venus", "mars", "terra", "jupiter", "saturn", "uranus", "neptun"]);
    assert_eq!(utils::remove_last(&x), vec!["merkur", "venus", "mars", "terra", "jupiter", "saturn", "uranus"]);
    let x: Vec<f64> = vec![];
    assert_eq!(utils::remove_first(&x), Vec::<f64>::new());
    assert_eq!(utils::remove_last(&x), Vec::<f64>::new());
}

// ----------------------------------------------------------------
// Test vector-to-set-conversions
// ----------------------------------------------------------------

#[test]
pub fn test_vec_to_set() {
    let x = vec!["b", "a", "a", "d", "d", "c", "b", "a"];
    let y1 = vec!["a", "a", "a", "b", "b", "c", "d", "d"];
    let y2 = vec!["d", "c", "b", "a", "d"];
    let z = vec!["b", "e", "f"];
    assert_eq!(utils::vec_to_set(&x), utils::vec_to_set(&y1), "Should match when both lists have similar elements");
    assert_eq!(utils::vec_to_set(&x), utils::vec_to_set(&y2), "Should match when both lists have similar elements, regardless of counts");
    assert_ne!(utils::vec_to_set(&x), utils::vec_to_set(&z), "Should not match when lists different elements");
}

#[test]
pub fn test_vec_to_multiset() {
    let x = vec!["b", "a", "a", "d", "d", "c", "b", "a"];
    let y1 = vec!["a", "a", "a", "b", "b", "c", "d", "d"];
    let y2 = vec!["d", "c", "b", "a", "d"];
    let z = vec!["b", "e", "f"];
    assert_eq!(utils::vec_to_multiset(&x), utils::vec_to_multiset(&y1), "Should match when both lists have similar elements with the same element counts");
    assert_ne!(utils::vec_to_multiset(&x), utils::vec_to_multiset(&y2), "Should not match when both lists have similar elements but different counts");
    assert_ne!(utils::vec_to_multiset(&x), utils::vec_to_multiset(&z), "Should not match when lists different elements");
}
