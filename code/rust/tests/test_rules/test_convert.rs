// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

use rstest::rstest;
use std::panic::catch_unwind;

use ads2::assert_length;
use ads2::vec_of_strings;

// ----------------------------------------------------------------
// Fixtures
// ----------------------------------------------------------------

// ----------------------------------------------------------------
// Test length
// ----------------------------------------------------------------

#[test]
fn test_vec_of_strings_creation() {
    assert!(catch_unwind(|| {
        let _: Vec<String> = vec_of_strings![];
        let _: Vec<String> = vec_of_strings!["apple", "bear", "coal"];
        let _: Vec<String> = vec_of_strings!["apple", "bear", "coal",];
    }).is_ok());
}

#[rstest]
#[case(vec_of_strings![], Vec::<&str>::new(), 0)]
#[case(vec_of_strings!["apple"], vec!["apple"], 1)]
#[case(vec_of_strings!["apple", "bear"], vec!["apple", "bear"], 2)]
#[case(vec_of_strings!["apple", -75.2], vec!["apple", "-75.2"], 2)]
#[case(vec_of_strings!["apple", "bear", "coal"], vec!["apple", "bear", "coal"], 3)]
#[case(vec_of_strings!["apple", "bear", "coal",], vec!["apple", "bear", "coal"], 3)]
fn test_vec_of_strings_cases(
    #[case] list1: Vec<String>,
    #[case] list2: Vec<&str>,
    #[case] n: usize)
{
    assert_length!(list1, n);
    assert_eq!(list1, list2);
}
