// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

use crate::core::utils;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CLASS Stack
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
 A data structure for stacks
 */
#[derive(Clone)]
pub struct Stack<T> {
    pub elements: Vec<T>,
    string: String,
}

impl<T> Stack<T>
where T: Clone + ToString + Eq {
    /// Creates new typed instance of stack.
    pub fn new() -> Stack<T> {
        return Stack {
            elements: Vec::new(),
            string: String::from(""),
        };
    }

    /// @returns number of elements in stack.
    pub fn len(self: &Self) -> usize {
        return self.elements.len();
    }

    /// adds element to stack
    pub fn push(self: &mut Self, x: T) {
        self.elements.push(x.clone());
    }

    /// obtains top element of stack without removing it
    pub fn top(self: &mut Self) -> T {
        let n = self.len();
        match self.elements.get(n-1) {
            Some(element) => { return element.clone(); },
            None =>  { panic!("Stack is empty!"); }
        };
    }

    /// obtains top element of stack and removes it
    pub fn pop(self: &mut Self) -> T {
        let element: T = self.top();
        self.elements = utils::remove_last::<T>(&mut self.elements);
        return element;
    }

    /// checks if element in stack:
    pub fn contains(self: &Self, element: &T) -> bool {
        return self.elements.contains(element);
    }

    /// convert entries to strings
    #[allow(dead_code)]
    fn repr(self: &Self) -> Vec<String> {
        return self.elements.iter()
            .map(|element| element.to_string())
            .collect::<Vec<String>>();
    }

    /// String representation
    #[allow(dead_code)]
    pub fn to_string(self: &mut Self) -> String {
        self.string = format!("{} |", self.repr().join("; "));
        return self.string.clone();
    }

    /// &str representation
    #[allow(dead_code)]
    pub fn as_string(self: &mut Self) -> &str {
        self.to_string();
        return self.string.as_str();
    }
}
