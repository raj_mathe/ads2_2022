// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

//

// ----------------------------------------------------------------
// METHODS log
// ----------------------------------------------------------------

/// Prints an info message
#[macro_export]
macro_rules! log_info{
    ($text:expr $(, $args:expr)* $(,)?)=>{
        print!("[\x1b[94;1mINFO\x1b[0m] ");
        println!($text$(, $args )*);
    }
}
#[allow(unused_imports)]
pub(crate) use log_info;

/// Prints a debug message
#[macro_export]
macro_rules! log_debug{
    ($text:expr $(, $args:expr)*  $(,)?)=>{
        print!("[\x1b[96;1mDEBUG\x1b[95;0m] ");
        println!($text$(, $args )*);
    }
}
#[allow(unused_imports)]
pub(crate) use log_debug;

// pub(crate) use log_debug;

/// Prints a warning message
#[macro_export]
macro_rules! log_warn{
    ($text:expr $(, $args:expr)*  $(,)?)=>{
        print!("[\x1b[93;1mWARNING\x1b[0m] ");
        println!($text$(, $args )*);
    }
}
#[allow(unused_imports)]
pub(crate) use log_warn;

/// Prints an error message
#[macro_export]
macro_rules! log_error{
    ($text:expr $(, $args:expr)*  $(,)?)=>{
        print!("[\x1b[91;1mERROR\x1b[0m] ");
        println!($text$(, $args )*);
    }
}
#[allow(unused_imports)]
pub(crate) use log_error;

/// Prints a fatal error message + crashes
#[macro_export]
macro_rules! log_fatal{
    ($text:expr $(, $args:expr)*  $(,)?)=>{
        panic!("[\x1b[91;1mFATAL\x1b[0m] {}", format!($text$(, $args )*));
    }
}

#[allow(unused_imports)]
pub(crate) use log_fatal;
