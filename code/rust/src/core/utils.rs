// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

extern crate regex;

use std::hash::Hash;
use std::collections::HashMap;
use std::collections::HashSet;

use self::regex::Regex;

// ----------------------------------------------------------------
// METHODS get regex
// ----------------------------------------------------------------

/// Constructs RegEx and panics if error.
#[allow(dead_code)]
pub fn construct_regex(pattern: &str) -> Regex {
    return Regex::new(pattern)
        .expect("Invalid regex construction!");
}

// ----------------------------------------------------------------
// METHODS values
// ----------------------------------------------------------------

#[macro_export]
macro_rules! value_min{
    ($value:expr $(,)?)=>{ $value };
    ($value:expr $(, $values:expr)+ $(,)?)=>{
        {
            let x = $crate::value_min!($( $values )+);
            if $value < x {
                $value
            } else {
                x
            }
        }
    };
}
#[allow(unused_imports)]
pub(crate) use value_min;

// ----------------------------------------------------------------
// METHODS Vectors
// ----------------------------------------------------------------

pub fn restrict<T>(x: &Vec<T>, i: usize, j: usize) -> Vec<T>
    where T: Clone
{
    return x[i..j].iter()
        .cloned()
        .collect::<Vec<T>>();
}

pub fn remove_last<T>(x: &Vec<T>) -> Vec<T>
    where T: Clone
{
    let n = x.len();
    if n == 0 {
        return Vec::<T>::new();
    }
    return restrict::<T>(x, 0, n-1);
}

#[allow(dead_code)]
pub fn remove_first<T>(x: &Vec<T>) -> Vec<T>
    where T: Clone
{
    let n = x.len();
    if n == 0 {
        return Vec::<T>::new();
    }
    return restrict::<T>(x, 1, n);
}

// ----------------------------------------------------------------
// METHODS Vectors to sets
// ----------------------------------------------------------------

#[allow(dead_code)]
pub fn vec_to_set<T>(x: &Vec<T>) -> HashSet<T>
    where T: Eq + Hash + Clone
{
    return x.iter()
        .map(|u| {u.clone()})
        .collect();
}

#[allow(dead_code)]
pub fn vec_to_multiset<T>(x: &Vec<T>) -> HashMap<T, usize>
    where T: Eq + Hash + Copy
{
    let mut counted = HashMap::<T, usize>::new();
    for u in x.iter() {
        *counted.entry(u.clone()).or_insert(0) += 1
    }
    return counted;
}
