// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

mod stacks;
mod core;
mod graphs;

use graphs::graph::Graph;
use graphs::tarjan::tarjan_algorithm;

// ----------------------------------------------------------------
// MAIN METHOD
// ----------------------------------------------------------------

fn main() {
    let nodes: Vec<_> = vec![1,2,3,4,5,6,7,8];
    let edges: Vec<(_, _)> = vec![
        (1,2),
        (1,3),
        (2,4),
        (2,5),
        (3,5),
        (3,6),
        (3,8),
        (4,5),
        (4,7),
        (5,1),
        (5,8),
        (6,8),
        (7,8),
        (8,6),
    ];
    let gph = Graph::new(nodes, edges);
    let components = tarjan_algorithm(&gph, true);
    println!("Components:");
    for component in components {
        println!("{:?}", component);
    }
}
