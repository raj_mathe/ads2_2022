// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

//

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Conversion macros - arrays
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/// Allows simpler inline creation of vectors of Strings
#[macro_export]
macro_rules! vec_of_strings{
    () => {
        Vec::<String>::new()
    };
    ($element:expr $(, $elements:expr )* $(,)?)=>{
        vec![ $element.to_string() $(, $elements.to_string() )* ]
    };
}
