// ----------------------------------------------------------------
// IMPORTS
// ----------------------------------------------------------------

//

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Assert macros - length
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#[macro_export]
macro_rules! assert_length{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert_eq!($a.len(), $b $(, $message)?);
    };
}

#[macro_export]
macro_rules! assert_length_ne{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert_ne!($a.len(), $b $(, $message)?);
    };
}

#[macro_export]
macro_rules! assert_length_lt{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert!($a.len() < $b $(, $message)?);
    };
}

#[macro_export]
macro_rules! assert_length_le{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert!($a.len() <= $b $(, $message)?);
    };
}

#[macro_export]
macro_rules! assert_length_gt{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert!($a.len() > $b $(, $message)?);
    };
}

#[macro_export]
macro_rules! assert_length_ge{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert!($a.len() >= $b $(, $message)?);
    };
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Assert macros - length of sets
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#[macro_export]
macro_rules! assert_length_unique{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert_eq!(vec_to_set(&$a).len(), $b $(, $message)?);
        }
    };
}

#[macro_export]
macro_rules! assert_length_unique_ne{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert_ne!(vec_to_set(&$a).len(), $b $(, $message)?);
        }
    };
}

#[macro_export]
macro_rules! assert_length_unique_lt{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert!(vec_to_set(&$a).len() < $b $(, $message)?);
        }
    };
}


#[macro_export]
macro_rules! assert_length_unique_le{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert!(vec_to_set(&$a).len() <= $b $(, $message)?);
        }
    };
}

#[macro_export]
macro_rules! assert_length_unique_gt{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert!(vec_to_set(&$a).len() > $b $(, $message)?);
        }
    };
}

#[macro_export]
macro_rules! assert_length_unique_ge{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert!(vec_to_set(&$a).len() >= $b $(, $message)?);
        }
    };
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Assert macros - contains
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#[macro_export]
macro_rules! assert_contains{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert!($a.contains(&$b) $(, $message)?);
    };
}

#[macro_export]
macro_rules! assert_not_contains{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        assert!(!$a.contains(&$b) $(, $message)?);
    };
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Assert macros - equality of sets
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#[macro_export]
macro_rules! assert_eq_contents{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert_eq!(vec_to_set(&$a), vec_to_set(&$b) $(, $message)?);
        }
    };
}

#[macro_export]
macro_rules! assert_ne_contents{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            use ads2::core::utils::vec_to_set;
            assert_ne!(vec_to_set(&$a), vec_to_set(&$b));
        }
    };
}

#[macro_export]
macro_rules! assert_subset{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            for element in $a.clone() {
                assert!($b.contains(&element) $(, $message)?);
            }
        }
    };
}

#[macro_export]
macro_rules! assert_not_subset{
    ($a:expr, $b:expr $(, $message:expr)?)=>{
        {
            let mut found = false;
            for element in $a.clone() {
                if !($b.contains(&element)) {
                    found = true;
                    break;
                }
            }
            assert!(found $(, $message)?);
        }
    };
}
