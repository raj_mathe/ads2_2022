# ADS2 - Implementierung in Rust #

Im Ordner [./src/*](src/) findet Module mit Datenstrukturen und Algorithmen.

Im Ordner [./tests/*](tests/) findet man _unit tests_,
die die verschiedenen Datenstrukturen und Algorithmen mit Testfälle belasten.
Man kann auch direkt im Code von [./src/main.rs](src/main.rs) aus
die Methoden mit Daten ausprobieren.

## Voraussetzungen ##

1. Der Rust-Compiler **`^1.60.*`** wird benötigt und
  hierfür ist der **`cargo`** Package-Manager zu empfehlen.
  Siehe <https://www.rust-lang.org/tools/install> für die offizielle Anleitung.
2. Es ist auch empfehlenswert, **`make`** zu installieren.
  - Linux/OSX: siehe <https://formulae.brew.sh/formula/make>.
  - Windows: siehe <https://community.chocolatey.org/packages/make>.

## Build -> Test -> Run ##

In einem IDE in dem Repo zu diesem Ordner navigieren.
</br>
Eine bash-Konsole aufmachen und folgende Befehle ausführen:

Wer **make** installiert hat:
```bash
# Zum Kompilieren (nur nach Änderungen nötig):
make build;
# Zur Ausführung der unit tests:
make tests;
# Zur Ausführung des Programms
make run;
# Zur Bereinigung aller Artefakte
make clean;
```
Wer _kein_ make hat:
```bash
# Zum Kompilieren (nur nach Änderungen nötig):
cargo build --release;
# Zur Ausführung der unit tests:
cargo test;
# Zur Ausführung des Programms:
./dist/ads2
# bzw. ads2.exe für Windows.
# Alternativ kann man den gebauten Artefakt per doppelklicken ausführen.
```
Der `build` Schritt baut einen binären Artefakt
und kopiert dies nach dem [./dist/*](dist/) Ordner.

Man kann auch mit einem guten Editor/IDE die Tests einzeln ausführen.
