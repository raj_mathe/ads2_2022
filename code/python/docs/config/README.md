# Documentation for Schemata for config models

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *http://.*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------


<a name="documentation-for-models"></a>
## Documentation for Models

 - [AppOptions](.//Models/AppOptions.md)
 - [AppOptionsHirschberg](.//Models/AppOptionsHirschberg.md)
 - [AppOptionsHirschbergMovePriorities](.//Models/AppOptionsHirschbergMovePriorities.md)
 - [AppOptionsRucksack](.//Models/AppOptionsRucksack.md)
 - [AppOptionsTarjan](.//Models/AppOptionsTarjan.md)
 - [Config](.//Models/Config.md)
 - [EnumHirschbergShow](.//Models/EnumHirschbergShow.md)
 - [EnumHirschbergVerbosity](.//Models/EnumHirschbergVerbosity.md)
 - [EnumLogLevel](.//Models/EnumLogLevel.md)
 - [EnumRucksackShow](.//Models/EnumRucksackShow.md)
 - [Info](.//Models/Info.md)


<a name="documentation-for-authorization"></a>
## Documentation for Authorization

All endpoints do not require authorization.
