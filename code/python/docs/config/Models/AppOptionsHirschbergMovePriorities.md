# AppOptionsHirschbergMovePriorities
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**diagonal** | [**Integer**](integer.md) |  | [optional] [default to 0]
**horizontal** | [**Integer**](integer.md) |  | [optional] [default to 1]
**vertical** | [**Integer**](integer.md) |  | [optional] [default to 2]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

