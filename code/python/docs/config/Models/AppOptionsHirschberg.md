# AppOptionsHirschberg
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**penalityMinusmismatch** | [**BigDecimal**](number.md) |  | [default to 1]
**penalityMinusgap** | [**BigDecimal**](number.md) |  | [default to 1]
**moveMinuspriorities** | [**AppOptions_hirschberg_move_priorities**](AppOptions_hirschberg_move_priorities.md) |  | [default to null]
**verbose** | [**List**](EnumHirschbergVerbosity.md) |  | [optional] [default to []]
**show** | [**List**](EnumHirschbergShow.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

