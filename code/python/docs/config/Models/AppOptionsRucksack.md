# AppOptionsRucksack
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**verbose** | [**Boolean**](boolean.md) |  | [optional] [default to false]
**show** | [**List**](EnumRucksackShow.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

