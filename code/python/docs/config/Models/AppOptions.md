# AppOptions
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**logMinuslevel** | [**EnumLogLevel**](EnumLogLevel.md) |  | [default to null]
**verbose** | [**Boolean**](boolean.md) | Global setting for verbosity. | [optional] [default to false]
**tarjan** | [**AppOptions_tarjan**](AppOptions_tarjan.md) |  | [default to null]
**tsp** | [**AppOptions_tarjan**](AppOptions_tarjan.md) |  | [default to null]
**hirschberg** | [**AppOptions_hirschberg**](AppOptions_hirschberg.md) |  | [default to null]
**rucksack** | [**AppOptions_rucksack**](AppOptions_rucksack.md) |  | [default to null]
**randomMinuswalk** | [**AppOptions_tarjan**](AppOptions_tarjan.md) |  | [default to null]
**genetic** | [**AppOptions_tarjan**](AppOptions_tarjan.md) |  | [default to null]
**euklid** | [**AppOptions_tarjan**](AppOptions_tarjan.md) |  | [default to null]
**pollardMinusrho** | [**AppOptions_tarjan**](AppOptions_tarjan.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

