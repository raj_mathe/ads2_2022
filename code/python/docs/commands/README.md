# Documentation for Schemata for command instructions

<a name="documentation-for-api-endpoints"></a>
## Documentation for API Endpoints

All URIs are relative to *http://.*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------


<a name="documentation-for-models"></a>
## Documentation for Models

 - [Command](.//Models/Command.md)
 - [CommandEuklid](.//Models/CommandEuklid.md)
 - [CommandGenetic](.//Models/CommandGenetic.md)
 - [CommandHirschberg](.//Models/CommandHirschberg.md)
 - [CommandPollard](.//Models/CommandPollard.md)
 - [CommandRandomWalk](.//Models/CommandRandomWalk.md)
 - [CommandRucksack](.//Models/CommandRucksack.md)
 - [CommandTarjan](.//Models/CommandTarjan.md)
 - [CommandTsp](.//Models/CommandTsp.md)
 - [DataTypeLandscapeGeometry](.//Models/DataTypeLandscapeGeometry.md)
 - [DataTypeLandscapeNeighbourhoods](.//Models/DataTypeLandscapeNeighbourhoods.md)
 - [DataTypeLandscapeValues](.//Models/DataTypeLandscapeValues.md)
 - [EnumAlgorithmNames](.//Models/EnumAlgorithmNames.md)
 - [EnumLandscapeMetric](.//Models/EnumLandscapeMetric.md)
 - [EnumOptimiseMode](.//Models/EnumOptimiseMode.md)
 - [EnumPollardGrowthRate](.//Models/EnumPollardGrowthRate.md)
 - [EnumRucksackAlgorithm](.//Models/EnumRucksackAlgorithm.md)
 - [EnumWalkMode](.//Models/EnumWalkMode.md)


<a name="documentation-for-authorization"></a>
## Documentation for Authorization

All endpoints do not require authorization.
