# CommandPollard
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**EnumAlgorithmNames**](EnumAlgorithmNames.md) |  | [default to null]
**number** | [**Integer**](integer.md) |  | [default to null]
**growth** | [**EnumPollardGrowthRate**](EnumPollardGrowthRate.md) |  | [default to null]
**xMinusinit** | [**Integer**](integer.md) |  | [optional] [default to 2]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

