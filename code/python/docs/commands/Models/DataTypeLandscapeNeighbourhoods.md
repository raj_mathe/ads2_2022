# DataTypeLandscapeNeighbourhoods
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**radius** | [**BigDecimal**](number.md) |  | [optional] [default to 1]
**metric** | [**EnumLandscapeMetric**](EnumLandscapeMetric.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

