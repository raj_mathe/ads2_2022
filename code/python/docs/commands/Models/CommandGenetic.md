# CommandGenetic
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**EnumAlgorithmNames**](EnumAlgorithmNames.md) |  | [default to null]
**population** | [**List**](array.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

