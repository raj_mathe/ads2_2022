# DataTypeLandscapeGeometry
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**neighbourhoods** | [**DataTypeLandscapeNeighbourhoods**](DataTypeLandscapeNeighbourhoods.md) |  | [default to null]
**labels** | [**List**](string.md) |  | [default to null]
**values** | [**DataTypeLandscapeValues**](DataTypeLandscapeValues.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

