# CommandRucksack
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**EnumAlgorithmNames**](EnumAlgorithmNames.md) |  | [default to null]
**algorithm** | [**EnumRucksackAlgorithm**](EnumRucksackAlgorithm.md) |  | [default to null]
**allowMinusfractional** | [**Boolean**](boolean.md) |  | [optional] [default to false]
**maxMinuscost** | [**BigDecimal**](number.md) | Upper bound for total cost of rucksack. | [default to null]
**costs** | [**List**](number.md) | Array of cost for each item (e.g. volume, weight, price, time, etc.). | [default to null]
**values** | [**List**](number.md) | Value extracted from each item (e.g. energy, profit, etc.). | [default to null]
**items** | [**List**](string.md) | Optional names of the items (if empty, defaults to 1-based indexes). | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

