# CommandHirschberg
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**EnumAlgorithmNames**](EnumAlgorithmNames.md) |  | [default to null]
**word1** | [**String**](string.md) | Word that gets placed vertically in algorithm. | [default to null]
**word2** | [**String**](string.md) | Word that gets placed horizontally in algorithm | [default to null]
**once** | [**Boolean**](boolean.md) |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

