# CommandRandomWalk
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**EnumAlgorithmNames**](EnumAlgorithmNames.md) |  | [default to null]
**algorithm** | [**EnumWalkMode**](EnumWalkMode.md) |  | [default to null]
**landscape** | [**DataTypeLandscapeGeometry**](DataTypeLandscapeGeometry.md) |  | [default to null]
**optimise** | [**EnumOptimiseMode**](EnumOptimiseMode.md) |  | [default to null]
**coordsMinusinit** | [**List**](integer.md) | Initial co-ordinates to start the algorithm. | [optional] [default to null]
**temperatureMinusinit** | [**Float**](float.md) |  | [optional] [default to null]
**annealing** | [**Boolean**](boolean.md) |  | [optional] [default to false]
**oneMinusbased** | [**Boolean**](boolean.md) |  | [optional] [default to false]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

