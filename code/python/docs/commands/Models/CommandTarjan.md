# CommandTarjan
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | [**EnumAlgorithmNames**](EnumAlgorithmNames.md) |  | [default to null]
**nodes** | [**List**](anyOf&lt;integer,number,string&gt;.md) |  | [default to null]
**edges** | [**List**](array.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

