#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os;
import sys

os.chdir(os.path.join(os.path.dirname(__file__)));
sys.path.insert(0, os.getcwd());

from src.models.config import *;
from src.core import log;
from src.setup import config;
from src import api;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MAIN METHOD
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def enter(*args: str):
    # set logging level:
    log.configure_logging(config.LOG_LEVEL);

    # process inputs:
    if len(args) == 0:
        # Führe befehle in Assets aus:
        for command in config.COMMANDS:
            result = api.run_command(command);
            # ignored if log-level >> DEBUG
            log.log_result(result, debug=True);
    else:
        # Führe CLI-Befehl aus:
        result = api.run_command_from_json(args[0]);
        # ignored if log-level >> DEBUG
        log.log_result(result, debug=True);
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXECUTION
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__ == '__main__':
    sys.tracebacklimit = 0;
    # NOTE: necessary for Windows, to ensure that console output is rendered correctly:
    os.system('');
    enter(*sys.argv[1:]);
