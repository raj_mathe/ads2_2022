#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from tests.thirdparty.unit import *;

from src.models.stacks import Stack;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Fixtures
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def format_data_for_display(data):
    return [ '{given_name} {family_name}: {title}'.format(**row) for row in data ];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test stack
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.usefixtures('test')
def test_stack_initialisation(test: TestCase):
    stack = Stack();
    test.assertEqual(len(stack), 0);

@mark.usefixtures('test')
def test_stack_push(test: TestCase):
    stack = Stack();
    test.assertEqual(len(stack), 0);
    stack.push('hallo');
    test.assertEqual(len(stack), 1);
    stack.push('welt');
    test.assertEqual(len(stack), 2);
    stack.push('!');
    test.assertEqual(len(stack), 3);

@mark.usefixtures('test')
def test_stack_no_error(test: TestCase):
    stack = Stack();
    stack.push('hallo');
    stack.push('welt');
    stack.push('!');
    with does_not_raise():
        stack.pop();
        stack.pop();
        stack.pop();

@mark.usefixtures('test')
def test_stack_error_po(test: TestCase):
    stack = Stack();
    stack.push('hallo');
    stack.push('welt');
    stack.push('!');
    with assert_raises(Exception):
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
