#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.log import *;
from src.thirdparty.misc import *;
from tests.thirdparty.unit import *;

from src.core.log import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# FIXTURES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@fixture(scope='module')
def message_1():
    return lorem.sentence();

@fixture(scope='module')
def message_2():
    return lorem.sentence();

@fixture(scope='module')
def message_3():
    return lorem.sentence();

@fixture(scope='module')
def message_4():
    return lorem.sentence();

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test log debug
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.usefixtures('message_1', 'message_2', 'message_3', 'message_4')
def test_log_debug_at_debug_level(
    test: TestCase,
    caplog: LogCaptureFixture,
    message_1: str,
    message_2: str,
    message_3: str,
    message_4: str,
):
    caplog.records.clear();
    caplog.set_level(logging.DEBUG);
    test.assertEqual(len(caplog.records), 0);
    log_debug(message_1);
    log_debug(message_2);
    log_debug(message_3);
    log_debug(message_4);
    # check that none of the 4 debug messages were suppressed:
    test.assertEqual(len(caplog.records), 4);
    test.assertEqual(caplog.records[0].levelname, 'DEBUG');
    test.assertEqual(caplog.records[0].message, message_1);
    test.assertEqual(caplog.records[1].levelname, 'DEBUG');
    test.assertEqual(caplog.records[1].message, message_2);
    test.assertEqual(caplog.records[2].levelname, 'DEBUG');
    test.assertEqual(caplog.records[2].message, message_3);
    test.assertEqual(caplog.records[3].levelname, 'DEBUG');
    test.assertEqual(caplog.records[3].message, message_4);
    return;

@mark.usefixtures('message_1', 'message_2', 'message_3', 'message_4')
def test_log_debug_at_info_level(
    test: TestCase,
    caplog: LogCaptureFixture,
    message_1: str,
    message_2: str,
    message_3: str,
    message_4: str,
):
    caplog.records.clear();
    caplog.set_level(logging.INFO);
    test.assertEqual(len(caplog.records), 0);
    log_debug(message_1);
    log_info(message_2);
    log_error(message_3);
    log_debug(message_4);
    # check that debug messages suppressed:
    test.assertNotEqual(len(caplog.records), 4);
    test.assertEqual(len(caplog.records), 2);
    test.assertEqual(caplog.records[0].levelname, 'INFO');
    test.assertEqual(caplog.records[1].levelname, 'ERROR');
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test log info
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.parametrize(('level',), [(logging.DEBUG,), (logging.INFO,)])
@mark.usefixtures('message_1', 'message_2', 'message_3', 'message_4')
def test_log_info(
    test: TestCase,
    caplog: LogCaptureFixture,
    message_1: str,
    message_2: str,
    message_3: str,
    message_4: str,
    level: int,
):
    caplog.records.clear();
    caplog.set_level(level);
    test.assertEqual(len(caplog.records), 0);
    log_info(message_1);
    log_info(message_2);
    log_info(message_3);
    log_info(message_4);
    # check that 4 info messages printed:
    test.assertEqual(len(caplog.records), 4);
    test.assertEqual(caplog.records[0].levelname, 'INFO');
    test.assertEqual(caplog.records[0].message, message_1);
    test.assertEqual(caplog.records[1].levelname, 'INFO');
    test.assertEqual(caplog.records[1].message, message_2);
    test.assertEqual(caplog.records[2].levelname, 'INFO');
    test.assertEqual(caplog.records[2].message, message_3);
    test.assertEqual(caplog.records[3].levelname, 'INFO');
    test.assertEqual(caplog.records[3].message, message_4);
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test log warn
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.parametrize(('level',), [(logging.DEBUG,), (logging.INFO,)])
@mark.usefixtures('message_1', 'message_2', 'message_3', 'message_4')
def test_log_warn(
    test: TestCase,
    caplog: LogCaptureFixture,
    message_1: str,
    message_2: str,
    message_3: str,
    message_4: str,
    level: int,
):
    caplog.records.clear();
    caplog.set_level(level);
    test.assertEqual(len(caplog.records), 0);
    log_warn(message_1);
    log_warn(message_2);
    log_warn(message_3);
    log_warn(message_4);
    # check that 4 warning messages printed:
    test.assertEqual(len(caplog.records), 4);
    test.assertEqual(caplog.records[0].levelname, 'WARNING');
    test.assertEqual(caplog.records[0].message, message_1);
    test.assertEqual(caplog.records[1].levelname, 'WARNING');
    test.assertEqual(caplog.records[1].message, message_2);
    test.assertEqual(caplog.records[2].levelname, 'WARNING');
    test.assertEqual(caplog.records[2].message, message_3);
    test.assertEqual(caplog.records[3].levelname, 'WARNING');
    test.assertEqual(caplog.records[3].message, message_4);
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test log error
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.parametrize(('level',), [(logging.DEBUG,), (logging.INFO,)])
@mark.usefixtures('message_1', 'message_2', 'message_3', 'message_4')
def test_log_error(
    test: TestCase,
    caplog: LogCaptureFixture,
    message_1: str,
    message_2: str,
    message_3: str,
    message_4: str,
    level: int,
):
    caplog.records.clear();
    caplog.set_level(level);
    test.assertEqual(len(caplog.records), 0);
    # assert that no sys-exit raised:
    with does_not_raise():
        log_error(message_1);
    with does_not_raise():
        log_error(message_2);
    with does_not_raise():
        log_error(message_3);
    with does_not_raise():
        log_error(message_4);
    # check that 4 error messages printed:
    test.assertEqual(len(caplog.records), 4);
    test.assertEqual(caplog.records[0].levelname, 'ERROR');
    test.assertEqual(caplog.records[0].message, message_1);
    test.assertEqual(caplog.records[1].levelname, 'ERROR');
    test.assertEqual(caplog.records[1].message, message_2);
    test.assertEqual(caplog.records[2].levelname, 'ERROR');
    test.assertEqual(caplog.records[2].message, message_3);
    test.assertEqual(caplog.records[3].levelname, 'ERROR');
    test.assertEqual(caplog.records[3].message, message_4);
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test log fatal
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.parametrize(('level',), [(logging.DEBUG,), (logging.INFO,)])
@mark.usefixtures('message_1', 'message_2', 'message_3', 'message_4')
def test_log_fatal(
    test: TestCase,
    caplog: LogCaptureFixture,
    message_1: str,
    message_2: str,
    message_3: str,
    message_4: str,
    level: int,
):
    caplog.records.clear();
    caplog.set_level(level);
    test.assertEqual(len(caplog.records), 0);
    # intercept sys-exist 1:
    with assert_raises(SystemExit) as caught:
        log_fatal(message_1);
    test.assertEqual(caught.value.code, 1);
    with assert_raises(SystemExit):
        log_fatal(message_2);
    with assert_raises(SystemExit):
        log_fatal(message_3);
    with assert_raises(SystemExit):
        log_fatal(message_4);
    # check that 4 critical messages printed:
    test.assertEqual(len(caplog.records), 4);
    test.assertEqual(caplog.records[0].levelname, 'CRITICAL');
    test.assertEqual(caplog.records[0].message, message_1);
    test.assertEqual(caplog.records[1].levelname, 'CRITICAL');
    test.assertEqual(caplog.records[1].message, message_2);
    test.assertEqual(caplog.records[2].levelname, 'CRITICAL');
    test.assertEqual(caplog.records[2].message, message_3);
    test.assertEqual(caplog.records[3].levelname, 'CRITICAL');
    test.assertEqual(caplog.records[3].message, message_4);
    return;
