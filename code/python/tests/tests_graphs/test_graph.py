#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from tests.thirdparty.unit import *;

from src.models.graphs import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# FIXTURES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@fixture(scope='module')
def graph1() -> Graph:
    return Graph(
        nodes=[1,2,3,4,5,6,7,8],
        edges=[(1,2), (1, 3), (2,3), (3,4), (4, 5), (5, 6), (6, 2), (6, 7), (6, 8)],
    );

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Test Graph
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@mark.usefixtures('test')
def test_graph_creation(test: TestCase):
    with does_not_raise():
        G = Graph(
            [5, 7, 8],
            [(5,7), (7, 8)]
        );
        test.assertEqual(len(G), 3);
        G = Graph(
            ["5", "7", "8", "10"],
            [("5", "7"), ("7", "8")]
        );
        test.assertEqual(len(G), 4);
        G = Graph(nodes=[], edges=[]);
        test.assertEqual(len(G), 0);

@mark.usefixtures('test', 'graph1')
def test_graph_subgraph(test: TestCase, graph1: Graph):
    sub_gph = graph1.subgraph([2,4,5,6,8]);
    test.assertCountEqual(sub_gph.edges, [(6,2), (4,5), (5,6), (6,8)]);

@mark.usefixtures('test', 'graph1')
def test_graph_successors_and_predecessors(test: TestCase, graph1: Graph):
    test.assertCountEqual(graph1.successors(1), [2, 3]);
    test.assertEqual(len(graph1.successors(8)), 0);
    test.assertCountEqual(graph1.successors(6), [2, 7, 8]);

    test.assertEqual(len(graph1.predecessors(1)), 0);
    test.assertCountEqual(graph1.predecessors(8), [6]);
    test.assertCountEqual(graph1.predecessors(6), [5]);
