#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.code import *;
from src.thirdparty.maths import *;

from models.generated.commands import *;
from src.core.calls import *;
from src.setup import config;
from src.algorithms.rucksack import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'endpoint_rucksack',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ENDPOINT
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@run_safely()
def endpoint_rucksack(command: CommandRucksack) -> Result[CallResult, CallError]:
    n = len(command.costs);
    assert len(command.values) == n, 'Number of values and costs must coincide!';
    assert len(command.items) in [0, n], f'Number of items must be 0 or {n}!';
    command.items = command.items or [ str(index + 1) for index in range(n) ];
    match command.algorithm:
        case EnumRucksackAlgorithm.greedy:
            result = rucksack_greedy_algorithm(
                max_cost = command.max_cost,
                costs = np.asarray(command.costs[:]),
                values = np.asarray(command.values[:]),
                items = np.asarray(command.items[:]),
                fractional = command.allow_fractional,
                verbose = config.OPTIONS.rucksack.verbose,
            );
        case EnumRucksackAlgorithm.branch_and_bound:
            result = rucksack_branch_and_bound_algorithm(
                max_cost = command.max_cost,
                costs = np.asarray(command.costs[:]),
                values = np.asarray(command.values[:]),
                items = np.asarray(command.items[:]),
                verbose = config.OPTIONS.rucksack.verbose,
            );
        case _ as alg:
            raise Exception(f'No algorithm implemented for {alg.value}.');
    return Ok(CallResult(action_taken=True, message=result));
