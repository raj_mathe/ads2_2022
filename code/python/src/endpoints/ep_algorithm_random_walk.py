#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.code import *;

from models.generated.commands import *;
from src.core.calls import *;
from src.setup import config;
from src.models.random_walk import *;
from src.algorithms.random_walk import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'endpoint_random_walk',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ENDPOINT
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@run_safely()
def endpoint_random_walk(command: CommandRandomWalk) -> Result[CallResult, CallError]:
    # Compute landscape (fitness fct + topology) + initial co-ordinates:
    one_based = command.one_based;
    landscape = Landscape(
        values = command.landscape.values,
        labels = command.landscape.labels,
        metric = command.landscape.neighbourhoods.metric,
        one_based = one_based,
    );
    if isinstance(command.coords_init, list):
        coords_init = tuple(command.coords_init);
        if one_based:
            coords_init = tuple(xx - 1 for xx in coords_init);
        assert len(coords_init) == landscape.dim, 'Dimension of initial co-ordinations inconsistent with landscape!';
    else:
        coords_init = landscape.coords_middle;

    match command.algorithm:
        case EnumWalkMode.adaptive:
            result = adaptive_walk_algorithm(
                landscape = landscape,
                r = command.landscape.neighbourhoods.radius,
                coords_init = coords_init,
                optimise = command.optimise,
                verbose = config.OPTIONS.random_walk.verbose
            );
        case EnumWalkMode.gradient:
            result = gradient_walk_algorithm(
                landscape = landscape,
                r = command.landscape.neighbourhoods.radius,
                coords_init = coords_init,
                optimise = command.optimise,
                verbose = config.OPTIONS.random_walk.verbose
            );
        case EnumWalkMode.metropolis:
            result = metropolis_walk_algorithm(
                landscape = landscape,
                r = command.landscape.neighbourhoods.radius,
                coords_init = coords_init,
                T = command.temperature_init,
                annealing = command.annealing,
                optimise = command.optimise,
                verbose = config.OPTIONS.random_walk.verbose
            );
        case _ as alg:
            raise Exception(f'No algorithm implemented for {alg.value}.');
    return Ok(CallResult(action_taken=True, message=result));
