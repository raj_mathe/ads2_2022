#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.types import *;
from src.thirdparty.maths import *;

from models.generated.config import *;
from src.models.hirschberg.penalties import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'represent_cost_matrix',
    'display_cost_matrix',
    'display_cost_matrix_halves',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def represent_cost_matrix(
    Costs: np.ndarray, # NDArray[(Any, Any), int],
    path: List[Tuple[int, int]],
    X: str,
    Y: str,
    verbose: List[EnumHirschbergVerbosity],
    pad: bool = False,
) -> np.ndarray: # NDArray[(Any, Any), Any]:
    m = len(X); # display vertically
    n = len(Y); # display horizontally

    # erstelle string-Array:
    if pad:
        table = np.full(shape=(3 + m + 3, 3 + n + 1), dtype=object, fill_value='');
    else:
        table = np.full(shape=(3 + m, 3 + n), dtype=object, fill_value='');

    # topmost rows:
    table[0, 3:(3+n)] = [ f'\x1b[2m{j}\x1b[0m' for j in range(n) ];
    table[1, 3:(3+n)] = [ f'\x1b[1m{y}\x1b[0m' for y in Y ];
    table[2, 3:(3+n)] = '--';
    # leftmost columns:
    table[3:(3+m), 0] = [ f'\x1b[2m{i}\x1b[0m' for i in range(m) ];
    table[3:(3+m), 1] = [ f'\x1b[1m{x}\x1b[0m' for x in X ];
    table[3:(3+m), 2] = '|';

    if pad:
        table[-3, 3:(3+n)] = '--';
        table[3:(3+m), -1] = '|';

    if EnumHirschbergVerbosity.costs in verbose:
        table[3:(3+m), 3:(3+n)] = Costs.copy();
        if EnumHirschbergVerbosity.moves in verbose:
            for (i, j) in path:
                table[3 + i, 3 + j] = f'\x1b[31;4;1m{table[3 + i, 3 + j]}\x1b[0m';
    elif EnumHirschbergVerbosity.moves in verbose:
        table[3:(3+m), 3:(3+n)] = '\x1b[2m.\x1b[0m';
        for (i, j) in path:
            table[3 + i, 3 + j] = '\x1b[31;1m*\x1b[0m';

    return table;

def display_cost_matrix(
    Costs: np.ndarray, # NDArray[(Any, Any), int],
    path: List[Tuple[int, int]],
    X: str,
    Y: str,
    verbose: EnumHirschbergVerbosity,
) -> str:
    '''
    Zeigt Kostenmatrix + optimalen Pfad.

    @inputs
    - `Costs` - Kostenmatrix
    - `Moves` - Kodiert die optimalen Schritte
    - `X`, `Y` - Strings

    @returns
    - eine 'printable' Darstellung der Matrix mit den Strings X, Y + Indexes.
    '''
    table = represent_cost_matrix(Costs=Costs, path=path, X=X, Y=Y, verbose=verbose);
    # benutze pandas-Dataframe + tabulate, um schöner darzustellen:
    repr = tabulate(pd.DataFrame(table), showindex=False, stralign='center', tablefmt='plain');
    return repr;

def display_cost_matrix_halves(
    Costs1: np.ndarray, # NDArray[(Any, Any), int],
    Costs2: np.ndarray, # NDArray[(Any, Any), int],
    path1: List[Tuple[int, int]],
    path2: List[Tuple[int, int]],
    X1: str,
    X2: str,
    Y1: str,
    Y2: str,
    verbose: EnumHirschbergVerbosity,
) -> str:
    '''
    Zeigt Kostenmatrix + optimalen Pfad für Schritt im D & C Hirschberg-Algorithmus

    @inputs
    - `Costs1`, `Costs2` - Kostenmatrizen
    - `Moves1`, `Moves2` - Kodiert die optimalen Schritte
    - `X1`, `X2`, `Y1`, `Y2` - Strings

    @returns
    - eine 'printable' Darstellung der Matrix mit den Strings X, Y + Indexes.
    '''
    table1 = represent_cost_matrix(Costs=Costs1, path=path1, X=X1, Y=Y1, verbose=verbose, pad=True);
    table2 = represent_cost_matrix(Costs=Costs2, path=path2, X=X2, Y=Y2, verbose=verbose, pad=True);

    # merge Taellen:
    table = np.concatenate([table1[:, :-1], table2[::-1, ::-1]], axis=1);

    # benutze pandas-Dataframe + tabulate, um schöner darzustellen:
    repr = tabulate(pd.DataFrame(table), showindex=False, stralign='center', tablefmt='plain');
    return repr;
