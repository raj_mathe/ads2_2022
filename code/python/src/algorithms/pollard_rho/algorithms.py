#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.types import *;
from src.thirdparty.maths import *;

from models.generated.config import *;
from src.core.utils import *;
from src.models.pollard_rho import *;
from src.algorithms.pollard_rho.display import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'pollard_rho_algorithm_linear',
    'pollard_rho_algorithm_exponential',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD pollard's rho algorithm - with linear grwoth
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def pollard_rho_algorithm_linear(
    n: int,
    x_init: int = 2,
    verbose: bool = False,
):
    steps = [];
    success = False;
    f = lambda _: fct(_, n=n);

    d = 1;
    x = y = x_init;
    steps.append(Step(x=x));
    k = 0;
    k_next = 1;

    while True:
        # aktualisiere x: x = f(x_prev):
        x = f(x);
        # aktualisiere y: y = f(f(y_prev)):
        y = f(f(y));

        # ggT berechnen:
        d = math.gcd(abs(x-y), n);
        steps.append(Step(x=x, y=y, d=d));

        # Abbruchkriterien prüfen:
        if d == 1: # weitermachen, solange d == 1
            k += 1;
            continue;
        elif d == n: # versagt
            success = False;
            break;
        else:
            success = True;
            break;

    if verbose:
        repr = display_table_linear(steps=steps);
        print('');
        print('\x1b[1mEuklidescher Algorithmus\x1b[0m');
        print('');
        print(repr);
        print('');
        if success:
            print('\x1b[1mBerechneter Faktor:\x1b[0m');
            print('');
            print(f'd = \x1b[1m{d}\x1b[0m.');
        else:
            print('\x1b[91mKein (Prim)faktor erkannt!\x1b[0m');
        print('');
    return d;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD pollard's rho algorithm - with exponential grwoth
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def pollard_rho_algorithm_exponential(
    n: int,
    x_init: int = 2,
    verbose: bool = False,
):
    steps = [];
    success = False;
    f = lambda _: fct(_, n=n);

    d = 1;
    x = y = x_init;
    steps.append(Step(x=x));
    k = 0;
    k_next = 1;

    while True:
        # aktualisiere x: x = f(x_prev):
        x = f(x);
        # aktualisiere y, wenn k = 2^j: y = x[j] = f(y_prev):
        if k == k_next:
            k_next = 2*k_next;
            y = f(y);

        # ggT berechnen:
        d = math.gcd(abs(x-y), n);
        steps.append(Step(x=x, y=y, d=d));

        # Abbruchkriterien prüfen:
        if d == 1: # weitermachen, solange d == 1
            k += 1;
            continue;
        elif d == n: # versagt
            success = False;
            break;
        else:
            success = True;
            break;

    if verbose:
        repr = display_table_exponential(steps=steps);
        print('');
        print('\x1b[1mEuklidescher Algorithmus\x1b[0m');
        print('');
        print(repr);
        print('');
        if success:
            print('\x1b[1mBerechneter Faktor:\x1b[0m');
            print('');
            print(f'd = \x1b[1m{d}\x1b[0m.');
        else:
            print('\x1b[91mKein (Prim)faktor erkannt!\x1b[0m');
        print('');
    return d;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# AUXILIARY METHOD function for Pollard's rho
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def fct(x: int, n: int) -> int:
    return (x**2 - 1) % n;
