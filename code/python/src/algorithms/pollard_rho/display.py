#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.code import *;
from src.thirdparty.maths import *;
from src.thirdparty.types import *;

from src.models.pollard_rho import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'display_table_linear',
    'display_table_exponential',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD display table - linear
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def display_table_linear(steps: List[Step]) -> str:
    table = pd.DataFrame({
        'i':  [i for i in range(len(steps))],
        'x':  [step.x for step in steps],
        'y':  [step.y or '-' for step in steps],
        'd':  [step.d or '-' for step in steps],
    }) \
    .reset_index(drop=True);
    # benutze pandas-Dataframe + tabulate, um schöner darzustellen:
    repr = tabulate(
        table,
        headers=['i', 'x(i)', 'y(i) = x(2i)', 'gcd(|x - y|,n)'],
        showindex=False,
        colalign=('right', 'right', 'right', 'center'),
        tablefmt='simple',
    );
    return repr;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHOD display table - exponential
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def display_table_exponential(steps: List[Step]) -> str:
    table = pd.DataFrame({
        'i':  [i for i in range(len(steps))],
        'x':  [step.x for step in steps],
        'y':  [step.y or '-' for step in steps],
        'd':  [step.d or '-' for step in steps],
    }) \
    .reset_index(drop=True);
    # benutze pandas-Dataframe + tabulate, um schöner darzustellen:
    repr = tabulate(
        table,
        headers=['i', 'x(i)', 'y(i) = x([log₂(i)])', 'gcd(|x - y|,n)'],
        showindex=False,
        colalign=('right', 'right', 'right', 'center'),
        tablefmt='simple',
    );
    return repr;
