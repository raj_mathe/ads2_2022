#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.config import *;

from models.generated.commands import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'command_from_json',
    'interpret_command',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS Convert to appropriate command type
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def command_from_json(command_json: str) -> Command:
    try:
        instructions = json.loads(command_json);
    except:
        raise Exception('Invalid json!');
    try:
        command = Command(**instructions);
    except:
        raise Exception('Invalid instruction format - consult schema!');
    command = interpret_command(command);
    return command;

def interpret_command(command: Command) -> Command:
    match command.name:
        case EnumAlgorithmNames.tarjan:
            return CommandTarjan(**command.dict());
        case EnumAlgorithmNames.tsp:
            return CommandTsp(**command.dict());
        case EnumAlgorithmNames.hirschberg:
            return CommandHirschberg(**command.dict());
        case EnumAlgorithmNames.rucksack:
            return CommandRucksack(**command.dict());
        case EnumAlgorithmNames.random_walk:
            return CommandRandomWalk(**command.dict());
        case EnumAlgorithmNames.genetic:
            return CommandGenetic(**command.dict());
        case EnumAlgorithmNames.euklid:
            return CommandEuklid(**command.dict());
        case EnumAlgorithmNames.pollard_rho:
            return CommandPollard(**command.dict());
    raise Exception(f'Command type `{command.name.value}` not recognised!');
