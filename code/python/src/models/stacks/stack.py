#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.types import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'Stack',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CLASS Stack
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Stack:
    '''
    A data structure for stacks
    '''
    elements: list[Any];

    def __init__(self):
        self.elements = [];
        return;

    def __len__(self):
        '''
        @returns
        number of elements in stack
        '''
        return len(self.elements);

    def __contains__(self, value: Any) -> bool:
        return value in self.elements;

    def __iter__(self) -> Generator[Any, None, None]:
        for value in self.elements:
            yield value;

    def __str__(self) -> str:
        return ', '.join([str(value) for value in self.elements[::-1]]);

    def push(self, value: Any):
        '''
        add element to stack
        '''
        self.elements.append(value);

    def top(self) -> Any:
        '''
        @returns
        top element from stack without removal
        '''
        if len(self.elements) == 0:
            raise Exception('Stack is empty!');
        return self.elements[-1];

    def pop(self) -> Any:
        '''
        @returns
        top element from stack and removes it
        '''
        value = self.top();
        self.elements = self.elements[:-1];
        return value;

    def contains(self, element: Any) -> bool:
        '''
        checks if element in stack:
        '''
        return element in self.elements;

    def empty(self) -> bool:
        return len(self) == 0;
