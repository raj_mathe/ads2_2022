#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.misc import *;
from src.thirdparty.config import *;
from src.thirdparty.code import *;
from src.thirdparty.types import *;

from models.generated.config import *;
from models.generated.commands import *;
from src.core.log import *;
from src.models.config import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'INFO',
    'OPTIONS',
    'COMMANDS',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PATH_ASSETS_CONFIG: str = 'assets/config.yaml';
PATH_ASSETS_COMMANDS: str = 'assets/commands.yaml';

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LAZY LOADED RESOURCES
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_assets_config(path: str) -> Config: # pragma: no cover
    with open(path, 'r') as fp:
        assets = yaml_load(fp, Loader=yaml_FullLoader);
        assert isinstance(assets, dict);
        return Config(**assets);

def create_commands(path: str) -> List[Command]: # pragma: no cover
    with open(path, 'r') as fp:
        assets = yaml_load(fp, Loader=yaml_FullLoader);
        return [
            interpret_command(Command(**instruction))
            for instruction in assets or []
        ];

# use lazy loaing to ensure that values only loaded (once) when used
CONFIG:    Config        = lazy(load_assets_config, path=PATH_ASSETS_CONFIG);
INFO:      Info          = lazy(lambda x: x.info, CONFIG);
OPTIONS:   AppOptions    = lazy(lambda x: x.options, CONFIG);
LOG_LEVEL: LOG_LEVELS    = lazy(log_level, OPTIONS);
COMMANDS:  List[Command] = lazy(create_commands, path=PATH_ASSETS_COMMANDS);
