#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.code import *;

from models.generated.commands import *;
from src.models.config import *
from src.endpoints import *;
from src.core.calls import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'run_command',
    'run_command_from_json',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# API METHODS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@run_safely(tag='api-from-json')
def run_command_from_json(command_json: str) -> Result[CallResult, CallError]:
    command = command_from_json(command_json);
    return run_command(command);

@run_safely(tag='api-from-command')
def run_command(command: Command) -> Result[CallResult, CallError]:
    if isinstance(command, CommandTarjan):
        return endpoint_tarjan(command);
    if isinstance(command, CommandTsp):
        return endpoint_tsp(command);
    elif isinstance(command, CommandHirschberg):
        return endpoint_hirschberg(command);
    elif isinstance(command, CommandRucksack):
        return endpoint_rucksack(command);
    elif isinstance(command, CommandRandomWalk):
        return endpoint_random_walk(command);
    elif isinstance(command, CommandGenetic):
        return endpoint_genetic(command);
    elif isinstance(command, CommandEuklid):
        return endpoint_euklid(command);
    elif isinstance(command, CommandPollard):
        return endpoint_pollard_rho(command);
    raise Exception(f'No endpoint set for `{command.name.value}`-command type.');
