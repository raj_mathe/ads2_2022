#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# IMPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from src.thirdparty.code import *;
from src.thirdparty.log import *;
from src.thirdparty.types import *;

from src.core.calls import *;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# EXPORTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

__all__ = [
    'LOG_LEVELS',
    'configure_logging',
    'log_info',
    'log_debug',
    'log_warn',
    'log_error',
    'log_fatal',
    'log_result',
    'log_dev',
];

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CONSTANTS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

_LOGGING_DEBUG_FILE: str = 'logs/debug.log';

class LOG_LEVELS(Enum): # pragma: no cover
    INFO  = logging.INFO;
    DEBUG = logging.DEBUG;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# METHODS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def configure_logging(level: LOG_LEVELS): # pragma: no cover
    logging.basicConfig(
        format = '[\x1b[1m%(levelname)s\x1b[0m] %(message)s',
        level  = level.value,
    );
    return;

def log_debug(*messages: Any):
    logging.debug(*messages);

def log_info(*messages: Any):
    logging.info(*messages);

def log_warn(*messages: Any):
    logging.warning(*messages);

def log_error(*messages: Any):
    logging.error(*messages);

def log_fatal(*messages: Any):
    logging.fatal(*messages);
    exit(1);

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Special Methods
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def log_result(result: Result[CallResult, CallError], debug: bool = False):
    '''
    Logs safely encapsulated result of call as either debug/info or error.

    @inputs
    - `result` - the result of the call.
    - `debug = False` (default) - if the result is okay, will be logged as an INFO message.
    - `debug = True` - if the result is okay, will be logged as a DEBUG message.
    '''
    if isinstance(result, Ok):
        value = result.unwrap();
        if debug:
            log_debug(asdict(value));
        else:
            log_info(asdict(value));
    else:
        err = result.unwrap_err();
        log_error(asdict(err));
    return;

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DEBUG LOGGING FOR DEVELOPMENT
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def log_dev(*messages: Any): # pragma: no cover
    with open(_LOGGING_DEBUG_FILE, 'a') as fp:
        print(*messages, file=fp);
