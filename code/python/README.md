# ADS2 - Implementierung in Python #

Im Ordner [./src/*](src/) findet Module mit Datenstrukturen und Algorithmen.

Im Ordner [./tests/*](tests/) findet man _unit tests_,
die die verschiedenen Datenstrukturen und Algorithmen mit Testfälle belasten.
Man kann auch direkt im Code von [./src/main.rs](src/main.rs) aus
die Methoden mit Daten ausprobieren.

## Voraussetzungen ##

1. Der Python-Compiler **`^3.10.*`** wird benötigt.
2. Das **`justfile`**-Tool wird benötigt (siehe <https://github.com/casey/just#installation>).

## Build -> Test -> Run ##

In einem IDE in dem Repo zu diesem Ordner navigieren.
</br>
Eine bash-Konsole aufmachen und folgende Befehle ausführen:

```bash
# Zeige alle Befehle:
just
# Zur Installation der Requirements (nur und immer nach Änderungen nötig):
just build
# Zur Ausführung der unit tests:
just tests
# Zur Ausführung des Programms
just run
# Zur Bereinigung aller Artefakte
just clean
```

Man kann auch mit einem guten Editor/IDE die Tests einzeln ausführen.

## Testfälle durch Config-Datei ##

Statt den Code immer anfassen zu müssen, kann man Fälle für die verschiedenen Algorithmen
in der **[./assets/commands.yaml](assets/commands.yaml)** erfassen und (mittels `just run`)
das Programm ausführen.

Weitere globale Einstellungen (z. B. über Verbosity, Penalty-Konstanten, usw.) kann man in
**[./assets/config.yaml](assets/config.yaml)** einstellen.
