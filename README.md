# Algorithmen und Datenstrukturen II, SoSe 2022 #

Dieses Repository ist für die Seminargruppe (c/d) am Donnerstag um 09:15–10:45.

In diesem Repository findet man:

- Protokolle der Seminargruppe [hier](./protocol).
- Notizen [hier](./notes).
- Symbolverzeichnis unter [notes/glossar.md](./notes/glossar.md).
- Referenzen unter [notes/quellen.md](./notes/quellen.md).

**HINWEIS 1:** In diesem Repository werden keine Personen bezogenen Daten der Studierenden gespeichert.

**HINWEIS 2:** Es besteht absolut gar keine Pflicht, die Materialien in diesem Repo zu verwenden.
Es handelt sich lediglich um zusätzliche Hilfsmittel.
**Im Zweifelsfalls** sollte man sich immer an den Definitionen und Auslegungen aus der VL orientieren.

## Leistungen + Zulassung ##

Siehe Moodle!

## Code ##

In den Unterordnern
    [`code/rust`](./code/rust)
    und
    [`code/python`](./code/python) (etwas ausführlicher)
sind Implementierungen von einigen Algorithmen bzw. Datenstrukturen zu finden sein.
